import random

def create_alphabet(words):
    alphabet = set()

    for word in words:
        for letter in word:
            alphabet.add(letter)

    return alphabet

def hamming_distance(s1, s2):
    return sum(c1 != c2 for c1, c2 in zip(s1, s2))


class Individual:
    def __init__(self, words, alphabet):
        self.alphabet = alphabet
        self.words = words

        word_size = len(words[0])
        self.code = [random.choice(list(self.alphabet)) for _ in range(word_size)]
        self.word = ''.join(self.code)
        self.hamming_distance = 0
        self.fitness = self.calcFitness()
        
    def __lt__(self, other):
        return self.fitness < other.fitness

    def __repr__(self):
        return f'{self.word}:{self.hamming_distance}'
    
    def __str__(self):
        return f'{self.word}:{self.hamming_distance}'
    
    def invert(self):
        random_index = random.randrange(len(self.code))
        self.code[random_index] = random.choice(self.alphabet)
    

    def calcFitness(self):
        self.hamming_distance = max([hamming_distance(self.word, w) for w in self.words]) 
        return 1 /  self.hamming_distance



def tournament_selection(population):
    TOURNAMENT_SIZE = 5
    
    bestFitness = float('-inf')
    
    index = -1

    for i in range(TOURNAMENT_SIZE):
        randomIndividual = random.randrange(len(population))

        if population[randomIndividual].fitness > bestFitness:
            bestFitness = population[randomIndividual].fitness
            index = randomIndividual
    
    return index

def roulette_selection(population):
  result = random.choices(range(len(population)), weights=[x.fitness for x in population], k = 1)
  return result[0]

def breakpoint_crossover(parent1, parent2, child1, child2):
    breakpoint = random.randrange(len(parent1.code))
    
    child1.code[:breakpoint] = parent1.code[:breakpoint]
    child2.code[:breakpoint] = parent2.code[:breakpoint]
    
    child1.code[breakpoint:] = parent2.code[breakpoint:]
    child2.code[breakpoint:] = parent1.code[breakpoint:]

    child1.word = "".join(child1.code) 
    child2.word = "".join(child2.code)    

def random_one_by_one_crossover(parent1, parent2, child1, child2):
    for i in range(len(parent1.code)):
        rand = random.random()
        if rand < 0.5:
            child1.code[i] = parent1.code[i]
            child2.code[i] = parent2.code[i]
        else:
            child1.code[i] = parent2.code[i]
            child2.code[i] = parent1.code[i]

    child1.word = "".join(child1.code) 
    child2.word = "".join(child2.code)          


def mutation(individual):
    MUTATION_PROB = 0.05
    for i in range(len(individual.code)):
        if random.random() < MUTATION_PROB:
            individual.code[i] = random.choice(list(individual.alphabet))

    individual.word = "".join(individual.code)

def read_instance(file_path):
    with open(file_path, 'r') as f:
        num_of_words = int(f.readline())
        _ = int(f.readline())
        _ = f.readline()

        words = []
        for _ in range(num_of_words):
            word = f.readline().strip()
            words.append(word)
        return words

if __name__ == '__main__':
    POPULATION_SIZE = 
    NUM_GENERATIONS = 30
    ELITISIM_SIZE = POPULATION_SIZE // 5

    words = read_instance('testing_examples/words_length_20/in/4.txt')
    alphabet = create_alphabet(words)

    selections = [tournament_selection, roulette_selection]
    crossovers = [breakpoint_crossover, random_one_by_one_crossover]
    for selection in selections:
        for crossover in crossovers:
            population = [Individual(words, alphabet) for _ in range(POPULATION_SIZE)]
            new_population = [Individual(words, alphabet) for _ in range(POPULATION_SIZE)]
            for i in range(NUM_GENERATIONS):
                population.sort(reverse=True)
                new_population[:ELITISIM_SIZE] = population[:ELITISIM_SIZE]

                for j in range(ELITISIM_SIZE, POPULATION_SIZE, 2):
                    parent1_index = selection(population)
                    parent2_index = selection(population)
                    
                    crossover(
                        population[parent1_index],
                        population[parent2_index],
                        new_population[j],
                        new_population[j+1]
                    )
                    
                    mutation(new_population[j])
                    mutation(new_population[j+1])
                    
                    new_population[j].fitness = new_population[j].calcFitness()
                    new_population[j+1].fitness = new_population[j+1].calcFitness()
                    
                population = new_population

            bestIndividual = max(population)
            print(f'solution: {bestIndividual.word}, hamming_distance: {bestIndividual.hamming_distance}')
