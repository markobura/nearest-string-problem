#include <iostream>
#include <vector>
#include <string>
#include <set>
#include <numeric>
#include <limits>
#include <queue>
#include <fstream>
#include <chrono>

std::set<char> create_alphabet(std::vector<std::string> &words) {
    std::set<char> alphabet;

    for (auto word : words) {
        for (auto letter : word) {
            alphabet.insert(letter);
        }
    }

    return alphabet;
}

int hamming_distance(std::string &s1, std::string &s2) {
    int count = 0;
    for (int i = 0; i < s1.length(); i++) {
        if (s1[i] != s2[i]) {
            count++;
        }
    }
    return count;
}

int calculate_value(std::string &string, std::vector<std::string> &references) {
    int max = 0;
    for (int i = 0; i < references.size(); i++) {
        int distance = hamming_distance(string, references[i]);
        if (distance > max) {
            max = distance;
        }
    }
    return max;
}

int read_instance(std::vector<std::string> &words, std::string path){
    std::fstream file(path);
    std::string line="";

    getline(file, line);
    int num_of_words = stoi(line);
    getline(file, line);
    int word_length = stoi(line);
    getline(file, line);

    for(int i=0; i< num_of_words; i++){
        getline(file, line);
        words.push_back(line);
    }
    
    return word_length;
}

int main(){
    std::vector<std::string> folders{
        "words_length_5",
        "words_length_10",
        "words_length_20"
    };

    for (auto &folder : folders) {
        std::cout << folder << ":\n";
        for (int i = 0; i < 4; i++) {
            auto start = std::chrono::high_resolution_clock::now();
            std::cout << std::to_string(i+1) + ".txt\n" ;
            std::vector<std::string> words;
            int word_length;
            word_length = read_instance(words, "testing_examples/" + folder +"/in/" + std::to_string(i+1) +".txt");
            std::set<char> alphabet = create_alphabet(words);
            std::queue<std::string> queue;
            queue.push("");
            int min_hamming = std::numeric_limits<int>::max();
            std::string target_string = "";
            int iterations = 0;
            std::string min_string = "";


            while (!queue.empty())
            {
                iterations +=1;
                std::string curr_word = queue.front();
                queue.pop();
                if (curr_word.size() == word_length){
                    int curr_word_value = calculate_value(curr_word, words);
                    if (curr_word_value < min_hamming){
                        min_hamming = curr_word_value;
                        min_string = curr_word;
                    }
                    continue;
                }

                for(auto next_letter : alphabet){
                    std::string new_word = curr_word + next_letter;
                    queue.push(new_word);
                }

            }

            auto stop = std::chrono::high_resolution_clock::now();
            auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start)/1e6;

            std::cout << "NSP: " << min_string << "\n";
            std::cout << "Hamming distance: " << min_hamming << "\n";
            std::cout << "Iterations: " << iterations << "\n";
            std::cout << "Time taken by algorithm: "
                << duration.count() << " seconds" << std::endl;
            std::cout << "--------------------------------------------------" << "\n\n";
        }

        std::cout << "==================================================" << "\n";
    }



    return 0;
}