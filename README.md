# Nearest String Problem

## :memo: Opis projekta
Problem najbliže niske (Nearest String Problem) je NP-težak problem u računarstvu koji pokušava da nađe geometrijsko središte skupa ulaznih reči.
<br>
Poenta projekta je implementacija genetskog algoritma sa različitim parametrima i analiza ponašanja implementiranih genetskih algoritama
<br><br>
U fajlu `brute_force.cpp` se nalazi rešenje pohlepne pretrage implementirano u jeziku C++. Ovo rešenje i izlaze koje je ono proizvelo korisćeni su za računanje tačnosti genetskih algoritama
<br>
<br>
U folderu `testing_examples` se nalaze testovi sa ulaznim vrednostima i rešenja koja je ispisao algoritam pohlepne pretrage, sa analizom rešenja i vremena.
<br>
<br>
U fajlu `genetic_algorithm.cpp` je implementiran i testiran genetski algoritam sa različitim selekcijama i ukrštanjem.
<br>
<br>
U fajlu `genetic_algorithm.ipynb` je isti genetski algoritam samo je predstavljen u Jupyter Notebook zbog iscrtavanja grafika i vizuelnog prikaza.
<br>
<br>
## Developers
- [Jelena Čanković, 96/2018](https://gitlab.com/jelena1406)
- [Marko Bura, 141/2018](https://gitlab.com/markobura)
